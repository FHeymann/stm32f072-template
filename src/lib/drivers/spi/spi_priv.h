/**
 * @file
 *
 *****************************************************************************
 * @title   spi_priv.h
 * @author  Daniel Schnell (deexschs)
 *
 * @brief   SPI private data structures
 *
 *******************************************************************************
 */

#ifndef __SPI_PRIV_H__
#define __SPI_PRIV_H__

#include <stdint.h>
#include <sys/types.h>
#include <stdbool.h>

#include "stm32f0xx.h"
#include "stm32f0xx_dma.h"
#include "stm32f0xx_spi.h"
#include "stm32f0xx_conf.h"

#include "FreeRTOS.h"
#include "semphr.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*
 * Defines
 */

/*
 * We have either DMA or CPU based SPI transfer. By defining SPI_USES_DMA, transfer is done via DMA
 * channel 2+3 (for SPI1) or 4+5 (for SPI2). If macro is undefined, CPU based IRQ handling is active.
 */
#define SPI_USES_DMA        (true)
#define P_UNUSED            __attribute__((__unused__))

/*
 * Structure definitions
 */

typedef volatile struct
{
    SPI_TypeDef* SPIx;

    // book keeping
    size_t rx_cnt;
    size_t tx_cnt;

    size_t rx_pos;
    size_t tx_pos;

    bool rx_in_use;
    bool tx_in_use;

    // buffers are provided by user
    char* rx_buf;
    char* tx_buf;

    // HW configuration
    GPIO_TypeDef* GPIOx;
    GPIO_TypeDef* GPIOx_nss;
    uint32_t GPIO_Pins;
    uint32_t NSS;
    bool is_partial_transfer;
    bool is_master;

    uint8_t NVIC_IRQChannel;
    uint8_t NVIC_IRQChannelPrio;

    char* transfer_buf_ptr;
    size_t transfer_buf_size;
#ifdef SPI_USES_DMA
    uint32_t dma_addr;
    DMA_Channel_TypeDef* dma_channel_rx;
    DMA_Channel_TypeDef* dma_channel_tx;
#endif

    // sync
    xSemaphoreHandle rx_sem;
    xSemaphoreHandle tx_sem;
    xSemaphoreHandle lock;
} DiagSPI;

extern volatile DiagSPI* spi1;
extern volatile DiagSPI* spi2;

void spi_chip_select(SPI_TypeDef* SPIx);
void spi_chip_deselect(SPI_TypeDef* SPIx);
void spi_rx_fifo_clear(DiagSPI* spi);

#ifdef __cplusplus
}
#endif

#endif /* __SPI_PRIV_H__ */

/* EOF */
